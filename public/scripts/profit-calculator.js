class ProfitCalculator {
    static calculate_profit_report(coins_data, initial_investment, power_cost) {
        report_obj["initial_investment"] = initial_investment;

        var day_len = 24 * 60 * 60 * 1000;
        var date_now = new Date();
        var time_offset = date_now.getTimezoneOffset()  * 60 * 1000;
        var unix_now = date_now.getTime();

        var unix_curr = unix_now + day_len - unix_now % day_len + time_offset;

        for (var key in coins_data) {
            if (key == "rate")
                continue;

            var curr_date = new Date(unix_curr);
        
            var curr_month = date_now.getMonth();
            var year = date_now.getFullYear();
            
            var speed = coins_data[key]["speed"];
            var price = coins_data[key]["price"];
            var power_consumption = coins_data[key]["power_consumption"];

            var mined = speed[0] * (unix_curr - unix_now) / 1000;
            var power_costs_speed = power_consumption * power_cost;
            var power_costs = power_costs_speed * (unix_curr - unix_now)  
                / 1000 / 60 / 60;
            var power_costs_total = power_costs;
            
            var revenue_currency = mined * price[0];
            var mined_total = 0;
            var revenue_currency_total = -initial_investment;

            var count = 1;

            var start_date = new Date(date_now.getTime());

            var report_arr = new Array();

            while (true) {
                if (curr_date.getMonth() != curr_month) {
                    mined_total += mined;
                    revenue_currency_total += revenue_currency;
                    power_costs_total += power_costs;

                    report_arr.push({
                        from: new Date(start_date).getDate(),
                        to: new Date(start_date.getFullYear(), 
                            start_date.getMonth() + 1, 0).getDate(),
                        month: new Date(start_date).getMonth(),
                        year: year,
                        mined: mined,
                        mined_total: mined_total,
                        revenue_currency: revenue_currency,
                        revenue_currency_total: revenue_currency_total,
                        power_costs: power_costs,
                        power_costs_total: power_costs_total
                    });

                    mined = revenue_currency = power_costs = 0;
                    curr_month = curr_date.getMonth();
                    year = curr_date.getFullYear();
                    start_date = new Date(curr_date.getTime()); 
                }

                curr_date = new Date(curr_date.getTime() + day_len);                                                       

                if (curr_date.getDate() == date_now.getDate() &&
                    curr_date.getMonth() == date_now.getMonth())
                    break;

                var daily_mined = 24*60*60*speed[count];
                mined += daily_mined;
                power_costs += power_costs_speed * 24;
                revenue_currency += daily_mined * price[count];
                count++;
            }

            mined += speed[364] * (unix_now % day_len) / 1000;
            revenue_currency += mined * price[364];
            power_costs += power_costs_speed * (unix_now % day_len)  / 1000 / 60 / 60;
            power_costs_total += power_costs;
            
            mined_total += mined;                    
            revenue_currency_total += revenue_currency;

            report_arr.push({
                from: new Date(start_date).getDate(),
                to: new Date(start_date.getFullYear(), 
                    start_date.getMonth() + 1, 0).getDate(),
                month: new Date(start_date).getMonth(),
                year: year,
                mined: mined,
                mined_total: mined_total,
                revenue_currency: revenue_currency,
                revenue_currency_total: revenue_currency_total,
                power_costs: power_costs,
                power_costs_total: power_costs_total
            });
                
            report_obj[key] = report_arr;
        }

        return report_obj;
    }
}