class GpuEdit extends HTMLElement {
    constructor() {
        super();
    }

    set_active() {
        this.button$.removeClass("btn-danger");
        this.button$.addClass("btn-success");
        this.setAttribute("data-gpu-active", true);
    }

    set_unactive() {
        this.button$.removeClass("btn-success");
        this.button$.addClass("btn-danger");
        this.setAttribute("data-gpu-active", false);
    }

    is_active() {
        return this.getAttribute("data-gpu-active") == "true";
    }

    connectedCallback() {
        var me = this;

        var shadow = this.attachShadow({mode: 'open'});
        var shadow$ = $(shadow)
        var style$ = $('<link rel="stylesheet" type="text/css"' + 
            ' href="/styles/bootstrap/css/bootstrap.min.css">');

        this.wrapper_div$ = $("<div></div>");
        this.group$ = $('<div class="input-group"></div>');
        this.number_text$ = $('<input type="text" class="form-control"/>');
        this.upd_callback = null;

        this.number_text$.on("input", function(event) {
            var target$ = $(event.target);
            var str = target$.val();
            var abc = "0123456789";
            var last = str.substr(-1, 1);
            if (abc.indexOf(last) < 0) {
                str = str.substr(0, str.length - 1);
                target$.val(str);   
            }

            if (me.upd_callback != null)
                me.upd_callback();
        });
        this.group$.append(this.number_text$);
        this.span$ = $('<span class="input-group-btn"></span>');
        this.button$ = $('<button class="btn btn-danger" type="button"></button>');
        this.setAttribute("data-gpu-active", false);
        
        this.button$.click(function(event) {
            var target$ = $(event.target);
            
            if (me.getAttribute("data-gpu-active") == "false")
                me.set_active();
            else 
                me.set_unactive();

            if (me.upd_callback != null)
                me.upd_callback();
        });
     
        shadow$.append(style$);
        this.span$.append(this.button$);
        this.group$.append(this.span$);
        this.wrapper_div$.append(this.group$);
        shadow$.append(this.wrapper_div$);
    }

    get count() {
        return this.number_text$.val();
    }

    set count(val) {
        this.number_text$.val(val);
    }

    set gpu_name(text) {
        this.button$.text(text);
        this.setAttribute("data-gpu-name", text);
    }

    get gpu_name() {
        return this.getAttribute("data-gpu-name");
    }
    
    add_hashrate(name, val) {
        var hashrate = parseFloat(val);
        this.setAttribute(`data-hashrate-${name}`, hashrate);
    }

    remove_hashrate(name) {
        this.removeAttribute(`data-hashrate-${name}`)
    }

    get_hashrate(name) {
        var val = parseFloat(this.getAttribute(`data-hashrate-${name}`));
        return isNaN(val)? 0 : val;
    }

    set update_callback(callback) {
        this.upd_callback = callback;
    }

    set power(val) {
        var power = parseFloat(val);
        power = isNaN(power)? 0 : power;
        this.setAttribute("data-power", power);
    } 

    get power() {
        return parseFloat(this.getAttribute("data-power"));
    }
}

customElements.define("gpu-edit", GpuEdit);