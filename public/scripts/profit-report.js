class ProfitReport extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.style.width = "100%";
        var shadow = this.attachShadow({mode: 'open'});
        var shadow$ = $(shadow)
        var style$ = $('<link rel="stylesheet" type="text/css"' + 
            ' href="/styles/bootstrap/css/bootstrap.min.css">');
        this.wrapper_div$ = $('<div style="style="overflow-x: auto; width:100%"></div>')
        this.report_table$ = $('<table class="table table-bordered"></table>');

        this.write_detalization_table = function(row$, report_obj, coin, currency, rate,
                resources) {
            var new_row$ = $("<tr></tr>");
            var cell$ = $('<td colspan="5"></td>');
            var div$ = $('<div style="overflow-x: auto; width:100%"></div>')
            var table$ = $('<table class="table table-bordered"></table>');
            var header$ = $('<thead></thead>');
            var header_row$ = $("<tr></tr>")
            var currency_smb = resources[`${currency}_smb`];
            
            header_row$.append(`<th>${resources["period"]}</th>`);
            header_row$.append(`<th>${resources["mined_per_period"]}</th>`);
            header_row$.append(`<th>${resources["profit_per_period"]} ${currency_smb}</th>`);
            header_row$.append(`<th>${resources["mined_total"]}</th>`);
            header_row$.append(`<th>${resources["total_profit"]} ${currency_smb}</th>`);
            header_row$.append(`<th>${resources["power_costs"]} ${currency_smb}</th>`);
            header_row$.append(`<th>${resources["total_power_costs"]} ${currency_smb}</th>`);
                
            header$.append(header_row$);
            var initial_investment = -report_obj["initial_investment"];
            var body$ =  $("<tbody></tbody>")
            body$.append(`<tr><td>${resources["at_the_begining"]}</td><td></td><td></td>` +
                `<td></td><td>${initial_investment} ` + 
                `(${resources["initial_investment"]})</td></td><td></td><td></td>`);
            
            var coin_report = report_obj[coin];

            var total_profit = initial_investment;
            
            for (var i = 0; i < coin_report.length; i++) {
                var month_number = coin_report[i].month;
                var from = coin_report[i].from;
                var to = coin_report[i].to;
                var year = coin_report[i].year;
                var month_name = resources["month_name"][month_number];
                var body_row$ = $("<tr></tr>");
                var period_cell$ = $(`<td>${from}-${to} ${month_name} ${year}</td>`)

                var mined_cell$ =$(`<td>${coin_report[i].mined.toFixed(3)}</td>`);
                var mined_total_cell$ = $(`<td>${coin_report[i].mined.toFixed(3)}</td>`);

                var revenue_currency = coin_report[i].revenue_currency; 
                var revenue_currency_total = coin_report[i].revenue_currency_total;

                var power_costs = coin_report[i].power_costs;
                var power_costs_total = coin_report[i].power_costs_total;

                var profit_currency = revenue_currency / rate - power_costs;
                var profit_currency_total = revenue_currency_total / rate - power_costs_total;

                var profit_cell$ = $(`<td>${profit_currency.toFixed(3)}</td>`);
                var profit_total_cell$ = $(`<td>${profit_currency_total.toFixed(3)}</td>`);

                var power_costs_cell$ = $(`<td>${power_costs.toFixed(3)}</td>`);
                var power_costs_total_cell$ = $(`<td>${power_costs_total.toFixed(3)}</td>`);
                
                body_row$.append(period_cell$);
                body_row$.append(mined_cell$);
                body_row$.append(profit_cell$);
                body_row$.append(mined_total_cell$);
                body_row$.append(profit_total_cell$);
                body_row$.append(power_costs_cell$);
                body_row$.append(power_costs_total_cell$);

                body$.append(body_row$);
            }

            table$.append(header$);
            table$.append(body$);
            div$.append(table$);
            cell$.append(div$);
            new_row$.append(cell$);
            row$.after(new_row$);
        }

        shadow$.append(style$);
        shadow$.append('<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">');
        this.wrapper_div$.append(this.report_table$);
        shadow$.append(this.wrapper_div$);
    }

    render_report_obj(report_obj, resources, currency, rate) {
        var me = this;

        var header$ = $('<thead></thead>');
        var currency_smb = resources[`${currency}_smb`];
        var header_row$ = $("<tr></tr>");

        header_row$.append($(`<th scope="col">${resources["name"]}</th>`));
        header_row$.append($(`<th scope="col">${resources["average_mined_per_day"]}` +
            "</th>"));
        header_row$.append($(`<th scope="col">${resources["average_profit_per_day"]}` +
            "</th>"));
        header_row$.append($(`<th scope="col">${resources["average_mined_per_month"]}` +
            "</th>"));
        header_row$.append($(`<th scope="col">${resources["average_profit_per_month"]} ` + 
            `${currency_smb}</th>`));
        header_row$.append(`<th scope="col">${resources["expected_profit_per_year"]} ` + 
            `${currency_smb}</th>`);
        header_row$.append("<th></th>");
        
        header$.append(header_row$);

        var average_profits = new Array();
        for (var key in report_obj) {
            if (key == "initial_investment")
                continue;

            var coin_report = report_obj[key];
            var total_mined = 0;
            var total_profit_currency = 0;
            var total_power_costs = 0;

            for (var i = 0; i < coin_report.length; i++) {
                total_mined += coin_report[i]["mined"]
                var profit = coin_report[i]["revenue_currency"] / rate 
                    - coin_report[i]["power_costs"]
                total_profit_currency += profit; 
                total_power_costs += coin_report[i]["power_costs"];
            }

            var obj_total = coin_report[coin_report.length - 1];
            var profit_total = obj_total["revenue_currency_total"] / rate - 
                obj_total["power_costs_total"];
            average_profits.push({
                coin: key,
                avg_mined_per_day: (total_mined / 365.0).toFixed(3),
                avg_mined_per_month: (total_mined / 12.0).toFixed(3),
                avg_power_costs_per_month: (total_power_costs / 12.0).toFixed(3),
                avg_profit_currency_per_day: (total_profit_currency / 
                    365.0).toFixed(3),
                avg_profit_currency_per_month: (total_profit_currency / 
                    12.0).toFixed(3),
                expected_profit_per_year: profit_total.toFixed(3)
            });
        }

        average_profits.sort(function(a, b) {
            if (a.expected_profit_per_year < b.expected_profit_per_year)
                return -1;
            else if (a.expected_profit_per_year > b.expected_profit_per_year)
                return 1;
            
            return 0;
        })
        
        var body$ = $("<tbody></tbody>");
        for (var i = 0; i < average_profits.length; i++) {
            var row$ = $("<tr></tr>");
            var coin = average_profits[i].coin;
            row$.append(`<td scope="row">${coin}</td>`);
            row$.append(`<td scope="row">${average_profits[i].avg_mined_per_day}</td>`);
            row$.append('<td scope="row">' +
                `${average_profits[i].avg_profit_currency_per_day}</td>`);
            row$.append(`<td scope="row">${average_profits[i].avg_mined_per_month}</td>`);
            row$.append('<td scope="row">' +
                `${average_profits[i].avg_profit_currency_per_month}</td>`);
            row$.append(`<td scope="row">${average_profits[i].expected_profit_per_year}` +
                "</td>");

            var button_cell$ = $('<td></td>');
            var button$ = $('<button type="button" class="btn btn-primary"' + 
                `data-coin=${coin}><i class="fas fa-caret-right"></i>&nbsp` + 
                `${resources["detalization"]}</button>`);
            button$.attr("data-detalization", "closed");
            button$.click(function(e) {
                var target$ = $(e.target);
                var icon$ = target$.children("i");
                var current_row$ = target$.parents("tr");
                var coin = target$.attr("data-coin");
                if (target$.attr("data-detalization") == "closed") {
                    target$.attr("data-detalization", "opened");
                    icon$.removeClass("fa-caret-right");
                    icon$.addClass("fa-caret-down");
                    me.write_detalization_table(current_row$, report_obj, coin, currency, 
                        rate, resources);
                } else {
                    target$.attr("data-detalization", "closed");
                    icon$.addClass("fa-caret-right");
                    icon$.removeClass("fa-caret-down");
                    current_row$.next().remove();
                }
            });
            button_cell$.append(button$);
            row$.append(button_cell$);
            body$.append(row$);
        }
        
        this.report_table$.append(header$);
        this.report_table$.append(body$);
    }
}

customElements.define("profit-report", ProfitReport);