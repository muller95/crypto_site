class HashrateEdit extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        var me = this;

        var shadow = this.attachShadow({mode: 'open'});
        var shadow$ = $(shadow)
        var style$ = $('<link rel="stylesheet" type="text/css"' + 
            ' href="/styles/bootstrap/css/bootstrap.min.css">');

        this.wrapper_div$ = $('<div></div>');
        this.group$ = $('<div class="input-group"></div>');

        this.hashrate_text$ = $('<input type="text" class="form-control"' + 
            'placeholder="Hashrate (H/s)"/>');
        this.on_input = null;
        this.hashrate_text$.on("input", function(event) {
            var target$ = $(event.target);
            var str = target$.val();
            var abc = "0123456789.";
            var last = str.substr(-1, 1);
            if (abc.indexOf(last) < 0) {
                str = str.substr(0, str.length - 1);
            }
            
            var str2 = "";
            var dot_found = false;
            for (var i = 0; i < str.length; i++) {
                if (str[i] == "." && dot_found)
                    continue;
                else if (str[i] == "." && !dot_found)
                    dot_found = true;

                str2 += str[i];
            }
            target$.val(str2); 

            if (me.on_input != null)
                me.on_input();
        });
        this.group$.append($(this.hashrate_text$));
        this.algorithm_span$ = $('<span class="input-group-addon"></span>');
        this.algorithm_span$.text(this.getAttribute("data-algorithm"));
        this.group$.append(this.algorithm_span$)
        this.group$.append(this.algorithm_div$)
        this.wrapper_div$.append(this.group$);

        shadow$.append(style$);
        shadow$.append(this.wrapper_div$);
    }

    set algorithm(text) {
        this.algorithm_span$.text(text);
        this.setAttribute("data-algorithm", text);
    }

    get algorithm() {
        return this.getAttribute("data-algorithm");
    }

    set hashrate(text) {
        this.hashrate_text$.val(text);
    }

    get hashrate() {
        var val = parseFloat(this.hashrate_text$.val());
        return isNaN(val)? 0 : val;
    }

    set on_input_callback(callback) {
        this.on_input = callback;
    } 
}

customElements.define("hashrate-edit", HashrateEdit);