#!/bin/bash
set -x

go get github.com/google/uuid
go get github.com/tarantool/go-tarantool
go get github.com/muller95/tntsessions
go get github.com/valyala/fasthttp

go build

cd tests/calc4miner-tests
mvn clean compile assembly:single
cd ../..

curl http://download.tarantool.org/tarantool/1.7/gpgkey | sudo apt-key add -
release=`lsb_release -c -s`

# install https download transport for APT
sudo apt-get -y install apt-transport-https

# append two lines to a list of source repositories
sudo rm -f /etc/apt/sources.list.d/*tarantool*.list
echo "deb http://download.tarantool.org/tarantool/1.7/ubuntu/ $release main" > /etc/apt/sources.list.d/tarantool_1_7.list
echo "deb-src http://download.tarantool.org/tarantool/1.7/ubuntu/ $release main" >> /etc/apt/sources.list.d/tarantool_1_7.list

# install
sudo apt-get update
sudo apt-get --allow-unauthenticated -y install tarantool

sudo apt-get install lua5.3
sudo apt-get install luarocks
sudo luarocks install luasocket
sudo luarocks install luasec
sudo luarocks install luafilesystem