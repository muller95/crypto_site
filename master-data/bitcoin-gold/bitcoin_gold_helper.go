package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func preapareFile(name string, multiplier float64) {
	file, err := os.Open("raw-" + name)
	if err != nil {
		log.Fatalf("Err opening raw-%s: %v\n", name, err)
	}
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Err reading raw-%s: %v\n", name, err)
	}

	fileStr := strings.Trim(string(fileBytes), " \r\n")
	fileStr = strings.Replace(fileStr, "\r", "", -1)
	lines := strings.Split(fileStr, "\n")[1:]
	file, err = os.Create(name)
	if err != nil {
		log.Fatalf("Err creating %s file: %v\n", name, err)
	}
	for i := 0; i < len(lines); i++ {
		line := lines[i]
		floatData, err := strconv.ParseFloat(line, 64)
		if err != nil {
			log.Fatalf("Err parsing float in file %s: %v\n", name, err)
		}
		fmt.Fprintf(file, "%.40f", floatData*multiplier)
		if i < len(lines)-1 {
			fmt.Fprintln(file)
		}
	}
}

func main() {
	preapareFile("hashrate.csv", 1000000)
	preapareFile("price.csv", 1)
	preapareFile("blocktime.csv", 60)
}
