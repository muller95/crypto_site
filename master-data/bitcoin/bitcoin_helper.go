package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func readFile(name string) []string {
	file, err := os.Open("raw-" + name)
	if err != nil {
		log.Fatalf("Err opening raw-%s: %v\n", name, err)
	}
	hashrateBytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Err reading raw-%s: %v\n", name, err)
	}

	hashrateFileStr := strings.Trim(string(hashrateBytes), " \r\n")
	hashrateFileStr = strings.Replace(hashrateFileStr, "\r", "", -1)
	runes := []rune(hashrateFileStr)
	runesClean := make([]rune, 0)
	comma := false
	for _, runeSmb := range runes {
		if runeSmb != ',' {
			comma = false
		} else if comma && runeSmb == ',' {
			continue
		} else if runeSmb == ',' {
			comma = true
		}

		runesClean = append(runesClean, runeSmb)
	}
	lines := strings.Split(string(runesClean), "\n")[1:]

	return lines
}

func preapareBlocktime(name string, multiplier float64) {
	lines := readFile(name)

	file, err := os.Create(name)
	if err != nil {
		log.Fatalf("Err creating %s file: %v\n", name, err)
	}
	for i := 0; i < len(lines); i++ {
		line := lines[i]
		parts := strings.Split(line, ",")
		dataStr := strings.Replace(parts[1], "\"", "", -1)
		floatData, err := strconv.ParseFloat(dataStr, 64)
		if err != nil {
			log.Fatalf("Err parsing float in file %s: %v\n", name, err)
		}
		fmt.Fprintf(file, "%.40f", floatData*multiplier)
		if i < len(lines)-1 {
			fmt.Fprintln(file)
		}
	}
}

func preapareHashrate(name string, multiplier float64) {
	lines := readFile(name)

	file, err := os.Create(name)
	if err != nil {
		log.Fatalf("Err creating %s file: %v\n", name, err)
	}
	for i := 0; i < len(lines); i++ {
		line := lines[i]
		line = strings.Trim(line, ",")
		parts := strings.Split(line, ",")
		floatDataTotal := 0.0
		for i := 1; i < len(parts); i++ {
			fmt.Println(parts[i])
			floatData, err := strconv.ParseFloat(parts[i], 64)
			if err != nil {
				log.Fatalf("Err parsing float in file %s: %v\n", name, err)
			}
			floatDataTotal += floatData
		}

		fmt.Fprintf(file, "%.40f", floatDataTotal*multiplier)
		if i < len(lines)-1 {
			fmt.Fprintln(file)
		}
	}
}

func preaparePrice(name string, multiplier float64) {
	lines := readFile(name)

	file, err := os.Create(name)
	if err != nil {
		log.Fatalf("Err creating %s file: %v\n", name, err)
	}
	for i := 0; i < len(lines); i++ {
		line := lines[i]
		line = strings.Trim(line, ",")
		parts := strings.Split(line, ",")
		floatDataTotal := 0.0
		for i := 1; i < len(parts); i++ {
			fmt.Println(parts[i])
			floatData, err := strconv.ParseFloat(parts[i], 64)
			if err != nil {
				log.Fatalf("Err parsing float in file %s: %v\n", name, err)
			}
			floatDataTotal += floatData
		}

		fmt.Fprintf(file, "%.40f", floatDataTotal*multiplier/float64(len(parts)-1))
		if i < len(lines)-1 {
			fmt.Fprintln(file)
		}
	}
}

func main() {
	// preapareFile("hashrate.csv", 1000000000000)
	preaparePrice("price.csv", 1)
	preapareHashrate("hashrate.csv", 1)
	preapareBlocktime("blocktime.csv", 60)
}
