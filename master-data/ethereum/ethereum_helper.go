package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func preapareFile(name string, multiplier float64) {
	file, err := os.Open("raw-" + name)
	if err != nil {
		log.Fatalf("Err opening raw-%s: %v\n", name, err)
	}
	hashrateBytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Err reading raw-%s: %v\n", name, err)
	}

	hashrateFileStr := strings.Trim(string(hashrateBytes), " \r\n")
	hashrateFileStr = strings.Replace(hashrateFileStr, "\r", "", -1)
	lines := strings.Split(hashrateFileStr, "\n")[1:]
	file, err = os.Create(name)
	if err != nil {
		log.Fatalf("Err creating %s file: %v\n", name, err)
	}
	for i := 0; i < len(lines); i++ {
		line := lines[i]
		parts := strings.Split(line, ",")
		dataStr := strings.Replace(parts[2], "\"", "", -1)
		floatData, err := strconv.ParseFloat(dataStr, 64)
		if err != nil {
			log.Fatalf("Err parsing float in file %s: %v\n", name, err)
		}
		fmt.Fprintf(file, "%.40f", floatData*multiplier)
		if i < len(lines)-1 {
			fmt.Fprintln(file)
		}
	}
}

func main() {
	preapareFile("hashrate.csv", 1000000000)
	preapareFile("price.csv", 1)
	preapareFile("blocktime.csv", 1)
}
