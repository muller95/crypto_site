helper = require "coinloaders.helper"
lunajson = require 'lunajson'

hashrate_lines, blocktime_lines, price_lines = helper.read_coin_params("bitcoin-gold")
print(#hashrate_lines)
print(#price_lines)
print(#blocktime_lines)

if arg[1] ~= "true" then 
    hashrate = tonumber(
        helper.do_https_request("https://www.btgblocks.com/api/getnetworkhashps"))
    
    response = helper.do_https_request(
        "https://api.coinmarketcap.com/v1/ticker/bitcoin-gold/")    
    response_json = lunajson.decode(response)
    print(response)

    hashrate_lines[#hashrate_lines + 1] = hashrate
    blocktime_lines[#blocktime_lines + 1] = 600
    price_lines[#price_lines + 1] = response_json[1].price_usd
end

print(#hashrate_lines)
print(#price_lines)
print(#blocktime_lines)

helper.write_params("bitcoin-gold", hashrate_lines, blocktime_lines, price_lines)