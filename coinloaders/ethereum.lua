helper = require "coinloaders.helper"
lunajson = require 'lunajson'


hashrate_lines, blocktime_lines, price_lines = helper.read_coin_params("ethereum")
print(#hashrate_lines)
print(#price_lines)
print(#blocktime_lines)

if arg[1] ~= "true" then 
    response = helper.do_https_request("https://api.ethermine.org/networkStats")
    response_json = lunajson.decode(response)
    print(response)
    if response_json.status == "OK" then 
        data = response_json.data
        hashrate = data.hashrate
        blocktime = data.blockTime
        price = data.usd

        hashrate_lines[#hashrate_lines + 1] = hashrate
        blocktime_lines[#blocktime_lines + 1] = blocktime
        price_lines[#price_lines + 1] = price
    end
end

print(#hashrate_lines)
print(#price_lines)
print(#blocktime_lines)

helper.write_params("ethereum", hashrate_lines, blocktime_lines, price_lines)