local helper={}

-- see if the file exists
local function file_exists(file)
    local f = io.open(file, "rb")
    if f then 
        f:close() 
    end
    return f ~= nil
end
  
  -- get all lines from a file, returns an empty 
  -- list/table if the file does not exist
local function read_file(file)
    if not file_exists(file) then 
        return nil 
    end

    lines = {}
    for line in io.lines(file) do 
        lines[#lines + 1] = line
    end
    return lines
end

function helper.read_coin_params(coinname)
    path = string.format("data/%s-price.csv", coinname)
    price_lines = read_file(path)
     if price_lines == nil then
        path = string.format("master-data/%s/price.csv", coinname)
        price_lines = read_file(path)
    end

    path = string.format("data/%s-hashrate.csv", coinname)
    hashrate_lines = read_file(path)
    if hashrate_lines == nil then
        path = string.format("master-data/%s/hashrate.csv", coinname)
        hashrate_lines = read_file(path)
    end

    path = string.format("data/%s-blocktime.csv", coinname)
    blocktime_lines = read_file(path)
    if blocktime_lines == nil then
        path = string.format("master-data/%s/blocktime.csv", coinname)
        blocktime_lines = read_file(path)
    end

    return hashrate_lines, blocktime_lines, price_lines
end

local function write_file(path, lines)
    lfs = require "lfs"
    file = io.open(path, "w")
    if file == nil then
        lfs.mkdir("data")
        file = io.open(path, "w")
        if file == nil then
            io.stderr:write("Err opening "..path.."\n")
            return        
        end
    end

    for i,line in ipairs(lines) do
        file:write(string.format("%.40f", tonumber(line)))
        if i < #lines then
            file:write("\n")
        end
    end
end

function helper.write_params(coinname, hashrate_lines, blocktime_lines, price_lines)
    path = string.format("data/%s-hashrate.csv", coinname)
    write_file(path, hashrate_lines)

    path = string.format("data/%s-blocktime.csv", coinname)
    write_file(path, blocktime_lines)

    path = string.format("data/%s-price.csv", coinname)
    write_file(path, price_lines)
end

function helper.do_https_request(path)
    local https = require 'ssl.https'
    local ltn12 = require("ltn12")
    req_headers = {}
    user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64) " .. 
        "AppleWebKit/537.11 (KHTML, like Gecko) " .. "Chrome/23.0.1271.95 Safari/537.11"
    t = {}
    res, code, headers, status = https.request {
        method = "GET",
        url = path,
        headers = { 
            ["User-Agent"] = user_agent 
        },
        sink = ltn12.sink.table(t)
    } 

    if code ~= 200 then 
        io.stderr:write("errcode " .. code ": " .. path.."\n")
        return nil
    end

    str = ""
    for k, v in pairs(t) do
        str = str .. v
    end

    return str
end
  
return helper