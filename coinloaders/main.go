package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
)

func main() {
	noUpdate := flag.Bool("noupdate", false, "switches off updating of coin data, "+
		"used to write master data to data directory")
	flag.Parse()
	noUpdateStr := fmt.Sprintf("%v", *noUpdate)

	bytes, err := ioutil.ReadFile("public/resources/mining/coins.json")
	if err != nil {
		log.Fatalf("coinloaders: Err reading coins json: %v\n", err)
	}

	var coins []string
	json.Unmarshal(bytes, &coins)

	for _, coin := range coins {
		cmd := exec.Command("lua", fmt.Sprintf("coinloaders/%s.lua", coin), noUpdateStr)
		cmd.Wait()
		outp, err := cmd.CombinedOutput()

		if err != nil {
			log.Printf("coinloaders: Err doing file %s.lua: %v\n", coin, err)
			continue
		}
		fmt.Printf("coinloaders/%s.lua\n", coin)
		fmt.Println(string(outp))
	}
}
