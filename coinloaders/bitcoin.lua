helper = require "coinloaders.helper"
lunajson = require 'lunajson'

hashrate_lines, blocktime_lines, price_lines = helper.read_coin_params("bitcoin")
print(#hashrate_lines)
print(#price_lines)
print(#blocktime_lines)

if arg[1] ~= "true" then 

    response = helper.do_https_request(
        "https://data.bitcoinity.org/export_data.csv?data_type" .. 
        "=block_time&f=m10&r=day&t=l&timespan=5y")
    response = response:gsub("^%s*(.-)%s*$", "%1")
    idx = response:match'^.*(),'
    blocktime = tonumber(response:sub(idx+1, string.len(response))) * 60
    print("blocktime "..blocktime)

    response = helper.do_https_request("https://data.bitcoinity.org/export_data.csv?" ..
        "c=m&data_type=hashrate&g=15&r=day&t=a&timespan=6m")
    response = response:gsub("^%s*(.-)%s*$", "%1")

    cleanstr = ''
    idx = response:match'^.*()\n' 
    response = response:sub(idx + 1, string.len(response))
    while response:sub(string.len(response),string.len(response)) == "," do
        response = response:sub(1, string.len(response) - 1)
    end

    cleanstr = ''
    comma = false
    for i=1,string.len(response) do
        smb = response:sub(i,i)
        if smb ~= "," then
            comma = false
            cleanstr = cleanstr .. smb
        elseif comma==false and smb == "," then
            comma = true
            cleanstr = cleanstr .. smb
        end
    end

    response = cleanstr

    idx = response:find(",")
    response = response:sub(idx + 1, response:len())
    hashrate = 0.0
    while true do
        idx = response:find(",")
        if idx ~= nil then
            number = response:sub(1, idx-1)
            response = response:sub(idx + 1, response:len())
            hashrate  = hashrate + tonumber(number)
        
        else 
            number = response
            hashrate  = hashrate + tonumber(number)
            break
        end

    end

    print(hashrate)


    response = helper.do_https_request("https://data.bitcoinity.org/export_data.csv?" .. 
        "c=e&currency=USD&data_type=price&r=day&t=l&timespan=6m")
    response = response:gsub("^%s*(.-)%s*$", "%1")

    cleanstr = ''
    idx = response:match'^.*()\n' 
    response = response:sub(idx + 1, string.len(response))
    while response:sub(string.len(response),string.len(response)) == "," do
        response = response:sub(1, string.len(response) - 1)
    end

    cleanstr = ''
    comma = false
    for i=1,string.len(response) do
        smb = response:sub(i,i)
        if smb ~= "," then
            comma = false
            cleanstr = cleanstr .. smb
        elseif comma==false and smb == "," then
            comma = true
            cleanstr = cleanstr .. smb
        end
    end

    response = cleanstr

    idx = response:find(",")
    response = response:sub(idx + 1, response:len())
    price = 0.0
    count = 0
    while true do
        idx = response:find(",")
        if idx ~= nil then
            count = count + 1
            number = response:sub(1, idx-1)
            response = response:sub(idx + 1, response:len())
            price  = price + tonumber(number)
        
        else 
            number = response
            count = count + 1
            price  = price + tonumber(number)
            break
        end

    end

    price = price / count

    hashrate_lines[#hashrate_lines + 1] = hashrate
    blocktime_lines[#blocktime_lines + 1] = blocktime
    price_lines[#price_lines + 1] = price
end

print(#hashrate_lines)
print(#price_lines)
print(#blocktime_lines)

helper.write_params("bitcoin", hashrate_lines, blocktime_lines, price_lines)