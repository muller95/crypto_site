package main

import (
	"fmt"
	"log"

	"github.com/muller95/tntsessions"
	"github.com/valyala/fasthttp"
)

func getRate(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	currency, err := sess.GetString("currency")
	if err != nil {
		log.Println("getCoinParams: Err on getting currency: ", err)
		ctx.Response.SetStatusCode(int(InternalServerError))
		return
	}

	ctx.Write([]byte(fmt.Sprintf("%v", ratesMap[currency])))
	ctx.Response.SetStatusCode(int(Ok))
}
