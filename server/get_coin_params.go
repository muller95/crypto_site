package main

import (
	"encoding/json"
	"io/ioutil"
	"log"

	"github.com/muller95/tntsessions"
	"github.com/valyala/fasthttp"
)

func getCoinParams(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	var hashrates map[string]map[string]float64
	coinParams := make(map[string]interface{})

	err := json.Unmarshal(ctx.QueryArgs().Peek("hashrates"), &hashrates)
	if err != nil {
		log.Printf("getCoinParams: Err parsing hashrates from user [%s]: %v\n",
			string(ctx.QueryArgs().Peek("hashrates")), err)
		ctx.Response.SetStatusCode(int(InternalServerError))
		return
	}

	var gpuAlgorithms map[string][]string
	bytes, _ := ioutil.ReadFile("public/resources/mining/gpu-coins.json")
	json.Unmarshal(bytes, &gpuAlgorithms)
	var asicAlgorithms map[string][]string
	bytes, _ = ioutil.ReadFile("public/resources/mining/asic-coins.json")
	json.Unmarshal(bytes, &asicAlgorithms)

	var rewards map[string]float64
	bytes, _ = ioutil.ReadFile("public/resources/mining/rewards.json")
	json.Unmarshal(bytes, &rewards)

	log.Println(gpuAlgorithms)
	log.Println(asicAlgorithms)

	for algorithm, request := range hashrates {
		hashrate := request["hashrate"]
		powerConsumption := request["power_consumption"]

		coins := gpuAlgorithms[algorithm]
		if len(coins) == 0 {
			coins = asicAlgorithms[algorithm]
			if len(coins) == 0 {
				log.Printf("getCoinParams: unknown algorithm %s", algorithm)
				ctx.Response.SetStatusCode(int(InternalServerError))
				return
			}
		}
		for _, coin := range coins {
			coinHashrate, err := readCoinParams("hashrate", coin)
			if err != nil {
				log.Printf("getCoinParams: Err pn reading hashrate for coin %s: %v\n", coin, err)
				ctx.Response.SetStatusCode(int(InternalServerError))
				return
			}
			coinBlockTime, err := readCoinParams("blocktime", coin)
			if err != nil {
				log.Printf("getCoinParams: Err on reading blocktime for coin %s: %v\n", coin, err)
				ctx.Response.SetStatusCode(int(InternalServerError))
				return
			}

			coinPrice, err := readCoinParams("price", coin)
			if err != nil {
				log.Printf("getCoinParams: Err on reading blocktime for coin %s: %v\n", coin, err)
				ctx.Response.SetStatusCode(int(InternalServerError))
				return
			}

			coinReward := rewards[coin]

			speed := make([]float64, len(coinHashrate))
			for i := 0; i < len(coinHashrate); i++ {
				speed[i] = hashrate / (coinHashrate[i] * coinBlockTime[i]) * coinReward
			}

			coinParams[coin] = map[string]interface{}{
				"price":             coinPrice,
				"speed":             speed,
				"power_consumption": powerConsumption,
			}
		}
	}

	coinParamsJSON, _ := json.Marshal(coinParams)
	ctx.Response.SetStatusCode(int(Ok))
	ctx.SetContentType("application/json")
	ctx.Write(coinParamsJSON)

}
