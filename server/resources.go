package main

import (
	"log"

	"github.com/muller95/tntsessions"
	"github.com/valyala/fasthttp"
)

func getResources(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	language := "en"
	isSet, err := sess.IsSet("language")
	if isSet && err == nil {
		language, _ = sess.GetString("language")
	} else if err != nil {
		log.Println("getResources: err getting language")
		ctx.Response.SetStatusCode(int(InternalServerError))
		return
	}

	ctx.SendFile("public/resources/language/" + language + ".json")
}
