package main

import (
	"log"
	"strings"

	"github.com/muller95/tntsessions"
	"github.com/valyala/fasthttp"
)

func setLanguage(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	language := string(ctx.PostArgs().Peek("language"))
	if language != "ru" {
		language = "en"
	}

	sess.Set("language", language)
	err := sessDB.Put(sess)
	if err != nil {
		log.Printf("Err on setting language: %v\n", err)
		ctx.Response.SetStatusCode(int(InternalServerError))
	}

	ctx.Response.SetStatusCode(int(Ok))
}

func getLanguage(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	isSet, err := sess.IsSet("language")
	language := "en"
	if err == nil && !isSet {
		acceptLanguages := strings.Split(string(ctx.Request.Header.Peek("Accept-Language")), ";")
		if len(acceptLanguages) > 0 {
			acceptLanguage := strings.Split(acceptLanguages[0], ",")
			if len(acceptLanguage) > 1 {
				if acceptLanguage[1] == "ru" {
					language = acceptLanguage[1]
				}
			}
		}
		sess.Set("language", language)
		err = sessDB.Put(sess)
		if err != nil {
			log.Println("getLanguage: err saving sess: ", err)
			ctx.Response.SetStatusCode(int(InternalServerError))
			return
		}
	} else if isSet && err == nil {
		language, _ = sess.GetString("language")
	} else if err != nil {
		log.Println("getLanguage: err checking 'language' existence: ", err)
		ctx.Response.SetStatusCode(int(InternalServerError))
		return
	}

	ctx.Response.SetStatusCode(int(Ok))
	ctx.Write([]byte(language))
}
