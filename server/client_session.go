package main

import (
	"log"

	"github.com/muller95/tntsessions"
	"github.com/valyala/fasthttp"
)

func saveClientSession(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	sess.Set("client_session", string(ctx.PostArgs().Peek("client_session")))
	err := sessDB.Put(sess)
	if err != nil {
		log.Printf("saveToSession: Err on saving data to session: %v\n", err)
		ctx.Response.SetStatusCode(int(InternalServerError))
	}
}

func getClientSession(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	clientSess := "{}"
	isSet, err := sess.IsSet("client_session")
	if err != nil {
		log.Printf("getClientSession: Err checking existence if 'client_session': %v\n", err)
		ctx.Response.SetStatusCode(int(InternalServerError))
		return
	} else if isSet {
		clientSess, _ = sess.GetString("client_session")
	}

	ctx.SetContentType("application/json")
	ctx.Response.SetStatusCode(int(Ok))
	ctx.Write([]byte(clientSess))
}
