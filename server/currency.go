package main

import (
	"log"

	"github.com/muller95/tntsessions"
	"github.com/valyala/fasthttp"
)

func setCurrency(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	currency := string(ctx.PostArgs().Peek("currency"))
	switch currency {
	case rubKey, usdKey:

	default:
		currency = usdKey
	}

	sess.Set("currency", currency)
	err := sessDB.Put(sess)
	if err != nil {
		log.Println("Err on setting currency: ", err)
	}
}

func getCurrency(ctx *fasthttp.RequestCtx, sess *tntsessions.Session) {
	isSet, err := sess.IsSet("currency")
	if err == nil && !isSet {
		sess.Set("currency", usdKey)
		err = sessDB.Put(sess)
		if err != nil {
			log.Println("getCurrency: err saving sess: ", err)
			ctx.Response.SetStatusCode(int(InternalServerError))
			return
		}
	} else if err != nil {
		log.Println("getCurrency: err checking 'currency' existence: ", err)
		ctx.Response.SetStatusCode(int(InternalServerError))
		return
	}

	currency, _ := sess.GetString("currency")
	ctx.SetStatusCode(int(Ok))
	ctx.Write([]byte(currency))
}
