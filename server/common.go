package main

import (
	"fmt"
	"log"
	"os"
)

func writeCryptoDataToFile(data []float64, name string) {
	n := 30
	file, err := os.Create(name)
	if err != nil {
		log.Printf("Err on creating file %s: %s\n", name, err)
		return
	}

	for i := len(data) - n; i < len(data); i++ {
		fmt.Fprintf(file, "%.40f", data[i])
		if i != len(data)-1 {
			fmt.Fprintf(file, "\n")
		}
	}

	file.Close()
}
