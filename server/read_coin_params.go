package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func readCoinParams(param, coin string) ([]float64, error) {
	outputData := make([]float64, 0, 365)

	bytes, err := ioutil.ReadFile(fmt.Sprintf("data/predict-%s-%s.csv", coin, param))
	if err != nil {
		return nil, fmt.Errorf("readCoinParams: err reading %s predictions for coin %s: %v",
			param, coin, err)
	}

	fileStr := strings.Trim(string(bytes), "\n")
	lines := strings.Split(fileStr, "\n")
	for lineNumber, line := range lines {
		val, err := strconv.ParseFloat(line, 64)
		if err != nil {
			return nil, fmt.Errorf("readCoinParams: err parsing float64 on "+
				"line %d for param %s for coin %s", lineNumber+1, param, coin)
		}

		outputData = append(outputData, val)
	}

	if len(outputData) < 365 {
		return nil, fmt.Errorf("readCoinParams: less then 365 lines "+
			"in %s predictions for coin %s", param, coin)
	}

	return outputData, nil
}
