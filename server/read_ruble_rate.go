package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func readRubleRate() (float64, error) {
	bytes, err := ioutil.ReadFile("data/ruble-rate.csv")
	if err != nil {
		return 0.0, fmt.Errorf("readRubleRate: err reading rate: %v", err)
	}

	fileStr := strings.Trim(string(bytes), "\n")
	val, err := strconv.ParseFloat(fileStr, 64)
	if err != nil {
		return 0.0, fmt.Errorf("readRubleRate: err parsing rate: %v", err)
	}
	return val, nil
}
