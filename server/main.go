package main

import (
	"fmt"
	"log"

	"github.com/muller95/tntsessions"
	"github.com/valyala/fasthttp"
)

type RestCode uint32

const (
	Ok                  RestCode = 200
	Forbidden           RestCode = 403
	NotFound            RestCode = 404
	SessionExpired      RestCode = 471
	InternalServerError RestCode = 500
)

var rubKey = "rub"
var usdKey = "usd"
var ratesMap = map[string]float64{
	rubKey: 0.0174,
	usdKey: 1.0,
}
var currencySmbs = map[string]string{
	rubKey: "₽",
	usdKey: "$",
}

var sessDB *tntsessions.SessionsBase

// var ethereumPrices []float64
var sessionLifeTime int64 = 3 * 24 * 60 * 60

func requestHandler(ctx *fasthttp.RequestCtx) {
	var err error

	sessID := string(ctx.Request.Header.Cookie("session_id"))
	sess, err := sessDB.Get(sessID)

	if err != nil && err != tntsessions.ErrNotFound && err != tntsessions.ErrSessionExpired {
		log.Printf("requestHandler: Err on getting session %v: %v\n", sessID, err)
		ctx.Response.SetStatusCode(int(InternalServerError))
		return
	} else if err == tntsessions.ErrNotFound || err == tntsessions.ErrSessionExpired {
		sess, err = sessDB.Create(sessionLifeTime)
		if err != nil {
			log.Printf("requestHandler: Err on creating session: %v\n", err)
			ctx.Response.SetStatusCode(int(InternalServerError))
			return
		}

		c := fasthttp.Cookie{}
		c.SetKey("session_id")
		c.SetValue(sess.ID)
		ctx.Response.Header.SetCookie(&c)

		sessDB.Put(sess)
		if err != nil {
			log.Println("requestHandler: err saving new session: ", err)
			ctx.Response.SetStatusCode(int(InternalServerError))
			return
		}
	}

	path := string(ctx.Path())
	switch path {
	case "/":
		ctx.SendFile("public/pages/main.html")

	case "/get_gpu_hashrates":
		ctx.SendFile("public/resources/hashrates/gpu.json")
	case "/get_gpu_coins":
		ctx.SendFile("public/resources/mining/gpu-coins.json")

	case "/get_asic_hashrates":
		ctx.SendFile("public/resources/hashrates/asic.json")
	case "/get_asic_coins":
		ctx.SendFile("public/resources/mining/asic-coins.json")

	case "/get_language":
		getLanguage(ctx, sess)

	case "/get_resources":
		getResources(ctx, sess)

	case "/get_currency":
		getCurrency(ctx, sess)

	case "/get_coin_params":
		getCoinParams(ctx, sess)

	case "/get_rate":
		getRate(ctx, sess)

	case "/set_language":
		setLanguage(ctx, sess)
	case "/set_currency":
		setCurrency(ctx, sess)
	case "/save_client_session":
		saveClientSession(ctx, sess)
	case "/get_client_session":
		getClientSession(ctx, sess)

		newRate, err := readRubleRate()
		if err != nil {
			log.Println("requestHandler: ", err)
			ctx.SetStatusCode(int(InternalServerError))
			return
		}
		ratesMap[rubKey] = newRate

		ctx.Response.SetStatusCode(int(Ok))
	default:
		fasthttp.FSHandler("public/", 0)(ctx)
	}
}

func main() {
	var err error

	sessDB, err = tntsessions.ConnectToTarantool("127.0.0.1:3309", "guest", "", "sessions")
	if err != nil {
		log.Fatalf("main: Err on connecting to sessions db: %v\n", err)
	}

	ratesMap[rubKey], err = readRubleRate()
	if err != nil {
		log.Fatalln("main: ", err)
	}

	fmt.Println("Server is ready")
	err = fasthttp.ListenAndServe("0.0.0.0:8080", requestHandler)

	// err = fasthttp.ListenAndServeTLS(":"+serverPort, certificatePath, keyPath,
	// requestHandler)
	if err != nil {
		log.Fatal("main: Err on startup server: ", err)
	}
}
