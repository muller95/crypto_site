package com.calc4miner.tests;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.file.FileAlreadyExistsException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

class Helper {
    public static JsonObject ReadJsonFromFile(String path) throws FileNotFoundException {
        FileInputStream fileIn = new FileInputStream(path);
        InputStreamReader in = new InputStreamReader(fileIn);
        JsonReader reader = new JsonReader(in);
        JsonParser parser = new JsonParser();
        JsonObject object = parser.parse(reader).getAsJsonObject();

        return object;
    }

    public  static WebElement GetCustomElementRoot(WebDriver driver, WebElement element) {
        WebElement root = (WebElement) ((JavascriptExecutor)driver)
            .executeScript("return arguments[0].shadowRoot", element);
        return root;
    }

    public static WebElement GetParent(WebDriver driver, WebElement element) {
        WebElement parent = (WebElement) ((JavascriptExecutor) driver)
            .executeScript("return arguments[0].parentNode;", element);
        return parent;
    }
    
}