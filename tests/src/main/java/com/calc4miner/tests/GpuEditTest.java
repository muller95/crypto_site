package com.calc4miner.tests;

import java.io.FileNotFoundException;
import java.util.Set;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

class GpuEditTest {
    public static void Test() throws Exception {
        WebDriver driver = new ChromeDriver();
        driver.get("http://crypto_site:8080");
        driver.manage().deleteAllCookies();
        
        JsonObject hashratesObject = Helper.ReadJsonFromFile(
            "../public/resources/hashrates.json");
            Set<Entry<String, JsonElement>> hashratesSet = hashratesObject.entrySet();

        WebElement hashrateInput = driver.findElement(By.id("gpu-hashrate"));
        WebElement powerConsumptionInput = driver.findElement(By.id("gpu-power-consumption"));
        WebElement coinEth = driver.findElement(By.cssSelector("input[data-coin='eth']"));
        coinEth = Helper.GetParent(driver, coinEth);
        WebElement coinZec = driver.findElement(By.cssSelector("input[data-coin='zec']"));
        coinZec = Helper.GetParent(driver, coinZec);
        String gpuTestInput = "-123abc.45";
        String gpuCheckInput = "12345";
        
        for (Entry<String, JsonElement> entry : hashratesSet) {
            String gpuName = entry.getKey();
            JsonObject gpuObject = entry.getValue().getAsJsonObject();
            String ethereum = gpuObject.get("ethereum").getAsString();
            String zcash = gpuObject.get("zcash").getAsString();
            String power = gpuObject.get("power").getAsString();

            String selector = String.format("gpu-edit[data-gpu-name='%s']" +
                "[data-ethereum-hashrate='%s'][data-zcash-hashrate='%s']" +
                "[data-power='%s']", gpuName, ethereum, zcash, power);
            WebElement element = driver.findElement(By.cssSelector(selector));
            WebElement root = Helper.GetCustomElementRoot(driver, element);
            WebElement button = root.findElement(By.cssSelector("button"));

            String gpuActive = element.getAttribute("data-gpu-active");
            if (!gpuActive.equals("false"))
                throw new Exception(String.format("Initial gpu-active is %s," +
                " expected false: %s", gpuActive, selector));
            
            button.click();
            gpuActive = element.getAttribute("data-gpu-active");
            if (!gpuActive.equals("true"))
                throw new Exception(String.format("First click gpu-active is %s," +
                " expected true: %s", gpuActive, selector));
            Thread.sleep(500);

            button.click();
            gpuActive = element.getAttribute("data-gpu-active");
            if (!gpuActive.equals("false"))
                throw new Exception(String.format("Second click gpu-active is %s," +
                " expected false: %s", gpuActive, selector));
            Thread.sleep(500);

            WebElement input = root.findElement(By.cssSelector("input"));
            input.sendKeys(gpuTestInput);
            if (!input.getAttribute("value").equals(gpuCheckInput))
                throw new Exception(String.format("Gpu number input expected %s, got %s: %s",
                    gpuCheckInput, input.getAttribute("value"), selector));
            
            button.click();
                
            long powerLong = Long.parseLong(power);
            long ethereumLong = Long.parseLong(ethereum);
            long zcashLong = Long.parseLong(zcash);
            long count = Long.parseLong(gpuCheckInput);

            long ethereumHashrate = count * ethereumLong;
            long zcashHashrate = count * zcashLong;
            double totalPower = powerLong * count / 1000.0;
            double powerInputDouble = Double.parseDouble(
                powerConsumptionInput.getAttribute("value"));
    
            if (totalPower != powerInputDouble)
                throw new Exception(String.format("Expected power %f, got %f: %s", 
                    totalPower, powerInputDouble, selector));
            
            coinZec.click();
            Thread.sleep(500);
            long hashrateInputLong = Long.parseLong(hashrateInput.getAttribute("value"));
            if (hashrateInputLong != zcashHashrate)
                throw new Exception(String.format("Expected zcash hashrate %d, got %d: %s",
                    zcashHashrate, hashrateInputLong, selector));

            coinEth.click();
            Thread.sleep(500);
            hashrateInputLong = Long.parseLong(hashrateInput.getAttribute("value"));
            if (hashrateInputLong != ethereumHashrate)
                throw new Exception(String.format("Expected ethereum hashrate %d, got %d: %s",
                    ethereumHashrate, hashrateInputLong, selector));

            Thread.sleep(500);
            
            for (int i = 0; i < gpuCheckInput.length(); i++)
                input.sendKeys(Keys.BACK_SPACE);

            if (!hashrateInput.getAttribute("value").equals(""))
                throw new Exception(String.format("Gpu hashrate is not clean, got %s",
                    hashrateInput.getAttribute("value")));

            if (!powerConsumptionInput.getAttribute("value").equals(""))
                throw new Exception(String.format("Gpu hashrate is not clean, got %s",
                    powerConsumptionInput.getAttribute("value")));
            
            button.click();
            Thread.sleep(500);
        }

        int i = 0;
        long totalEthereum = 0;
        long totalZcash = 0;
        double totalPower = 0;
        for (Entry<String, JsonElement> entry : hashratesSet) {
            String gpuName = entry.getKey();
            JsonObject gpuObject = entry.getValue().getAsJsonObject();
            String ethereum = gpuObject.get("ethereum").getAsString();
            String zcash = gpuObject.get("zcash").getAsString();
            String power = gpuObject.get("power").getAsString();

            String selector = String.format("gpu-edit[data-gpu-name='%s']" +
                "[data-ethereum-hashrate='%s'][data-zcash-hashrate='%s']" +
                "[data-power='%s']", gpuName, ethereum, zcash, power);
            WebElement element = driver.findElement(By.cssSelector(selector));
            WebElement root = Helper.GetCustomElementRoot(driver, element);
            WebElement button = root.findElement(By.cssSelector("button"));
            
            button.click();

            WebElement input = root.findElement(By.cssSelector("input"));
            input.sendKeys(String.format("%d", 2*(i%2)));
            
            long powerLong = Long.parseLong(power);
            long ethereumLong = Long.parseLong(ethereum);
            long zcashLong = Long.parseLong(zcash);
            long count = 2*(i%2);
            
            totalEthereum += count * ethereumLong;
            totalZcash += count * zcashLong;
            totalPower += count * powerLong;

            i++;
        }

        double powerInputDouble = Double.parseDouble(
                powerConsumptionInput.getAttribute("value"));
        totalPower /= 1000.0;
        if (totalPower != powerInputDouble)
            throw new Exception(String.format("Expected power %f, got %f", 
                totalPower, powerInputDouble));
        
        coinZec.click();
        Thread.sleep(500);
        long hashrateInputLong = Long.parseLong(hashrateInput.getAttribute("value"));
        if (hashrateInputLong != totalZcash)
            throw new Exception(String.format("Expected zcash hashrate %d, got %d",
                totalZcash, hashrateInputLong));

        coinEth.click();
        Thread.sleep(500);
        hashrateInputLong = Long.parseLong(hashrateInput.getAttribute("value"));
        if (hashrateInputLong != totalEthereum)
            throw new Exception(String.format("Expected ethereum hashrate %d, got %d",
                totalEthereum, hashrateInputLong));

        driver.quit();
    }
}