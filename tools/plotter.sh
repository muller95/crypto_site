#!/bin/bash

echo "ethereum hashrate"
plotter/plotter/bin/Debug/plotter.exe ../data/ethereum-hashrate.csv \
    ../data/predict-ethereum-hashrate.csv ethereum-hashrate

echo "ethereum blocktime"
plotter/plotter/bin/Debug/plotter.exe ../data/ethereum-blocktime.csv \
    ../data/predict-ethereum-blocktime.csv ethereum-blocktime

echo "ethereum price"
plotter/plotter/bin/Debug/plotter.exe ../data/ethereum-price.csv \
    ../data/predict-ethereum-price.csv ethereum-price    

echo "\n\n\n"

echo "zcash hashrate"
plotter/plotter/bin/Debug/plotter.exe ../data/zcash-hashrate.csv \
    ../data/predict-zcash-hashrate.csv zcash-hashrate

echo "zcash blocktime"
plotter/plotter/bin/Debug/plotter.exe ../data/zcash-blocktime.csv \
    ../data/predict-zcash-blocktime.csv zcash-blocktime

echo "zcash price"
plotter/plotter/bin/Debug/plotter.exe ../data/zcash-price.csv \
    ../data/predict-zcash-price.csv zcash-price 
