﻿using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;

namespace plotter
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			if (args.Length < 3) {
				Console.Error.WriteLine("Three argumenst are required");
			}

			double max = 0;
			double min = Double.PositiveInfinity;
			List<double> oldData = new List<double>();
			List<double> newData = new List<double>();

			StreamReader reader = new StreamReader(args[0]);
			while (!reader.EndOfStream) {
				double val = Convert.ToDouble(reader.ReadLine());
				oldData.Add(val);
				max = Math.Max(max, val);
				min = Math.Min (min, val);
			}
			reader.Close();

			reader = new StreamReader(args[1]);
			while (!reader.EndOfStream) {
				double val = Convert.ToDouble(reader.ReadLine());
				newData.Add(val);
				max = Math.Max(max, val);
				min = Math.Min (min, val);
			}
			reader.Close();

			double step = 15.0;
			double multiplier = 5000.0 / max;
			double width = step * (oldData.Count + newData.Count);

			Bitmap bitmap = new Bitmap((int)width, 5100);
			Graphics g = Graphics.FromImage(bitmap);

			g.Clear (Color.White);

			double position = 0;
		
			for (int i = 0; i < oldData.Count - 1; i++, position += step) {
				double val1 = oldData [i] * multiplier;
				double val2 = oldData [i + 1] * multiplier;
				g.DrawLine (new Pen (Color.Red, 1.5f), (float)position, (float)(5100 - val1), 
					(float)(position + step), (float)(5100 - val2));
			}

			for (int i = 0; i < newData.Count - 1; i++, position += step) {
				double val1 = newData [i] * multiplier;
				double val2 = newData [i + 1] * multiplier;
				g.DrawLine (new Pen (Color.Green, 1.5f), (float)position, (float)(5100 - val1), 
					(float)(position + step), (float)(5100 - val2));
			}

			Console.WriteLine ("Min: {0}", min);
			Console.WriteLine ("Max: {0}", max);

			bitmap.Save (args[2] + ".png");
		}
	}
}
