#!/bin/bash

set -x

cd server
go build
mv server ../crypto_site
cd .. 

#cd arima
#mvn clean compile assembly:single
#cd ..

cd coinloaders
go build
cd ..

cd goforecast
go build
cd ..