package main

import "math"

func invertibleMatrix(matrix [][]float64) [][]float64 {
	result := make([][]float64, len(matrix))
	mtx := make([][]float64, len(matrix))
	for i := 0; i < len(matrix); i++ {
		result[i] = make([]float64, len(matrix[i]))
		result[i][i] = 1
		mtx[i] = make([]float64, len(matrix[i]))
		for j := 0; j < len(mtx[i]); j++ {
			mtx[i][j] = matrix[i][j]
		}
	}

	for i := 0; i < len(mtx)-1; i++ {
		max := math.Abs(mtx[i][i])
		idx := i
		for j := i + 1; j < len(mtx); j++ {
			if math.Abs(mtx[j][i]) > max {
				max = math.Abs(mtx[j][i])
				idx = j
			}
		}

		tmpRow := mtx[i]
		mtx[i] = mtx[idx]
		mtx[idx] = tmpRow
		tmpRow = result[i]
		result[i] = result[idx]
		result[idx] = tmpRow
		for j := i + 1; j < len(mtx); j++ {
			mult := mtx[j][i] / mtx[i][i]
			for k := 0; k < len(mtx[j]); k++ {
				mtx[j][k] -= mult * mtx[i][k]
				result[j][k] -= mult * result[i][k]
			}
		}
	}

	for i := len(mtx) - 1; i > 0; i-- {
		for j := i - 1; j >= 0; j-- {
			mult := mtx[j][i] / mtx[i][i]
			for k := 0; k < len(mtx[j]); k++ {
				mtx[j][k] -= mult * mtx[i][k]
				result[j][k] -= mult * result[i][k]
			}
		}
	}

	for i := 0; i < len(mtx); i++ {
		for j := 0; j < len(result[i]); j++ {
			result[i][j] /= mtx[i][i]
		}
	}

	return result
}

func matrixMult(a, b [][]float64) [][]float64 {
	r := make([][]float64, len(a))
	for i := 0; i < len(a); i++ {
		r[i] = make([]float64, len(b[0]))
	}

	for i := 0; i < len(a); i++ {
		for j := 0; j < len(b[0]); j++ {
			for k := 0; k < len(a); k++ {
				r[i][j] += a[i][k] * b[k][j]
			}
		}
	}

	return r
}

func vectorDif(a, b []float64) []float64 {
	result := make([]float64, len(a))
	for i := 0; i < len(a); i++ {
		result[i] = a[i] - b[i]
	}

	return result
}

func vectorSum(a, b []float64) []float64 {
	result := make([]float64, len(a))
	for i := 0; i < len(a); i++ {
		result[i] = a[i] + b[i]
	}

	return result
}

func vectNumMult(v []float64, a float64) []float64 {
	result := make([]float64, len(v))
	for i := 0; i < len(v); i++ {
		result[i] = a * v[i]
	}

	return result
}

func matrixVecMult(m [][]float64, v []float64) []float64 {
	r := make([]float64, len(v))
	for i := 0; i < len(m); i++ {
		r[i] = 0
		for j := 0; j < len(v); j++ {
			r[i] += m[i][j] * v[j]
		}
	}

	return r
}

func norm2(x []float64) float64 {
	norm2 := 0.0
	for i := 0; i < len(x); i++ {
		norm2 += math.Pow(x[i], 2)
	}

	return math.Sqrt(norm2)
}
