package main

import "math"

func arnoldi(A, Q [][]float64, k int) ([]float64, []float64) {
	q := make([]float64, len(Q))
	qi := make([]float64, len(Q))
	h := make([]float64, k+2)
	for i := 0; i < len(Q); i++ {
		q[i] = Q[i][k]
	}

	q = matrixVecMult(A, q)
	for i := 0; i < k+2; i++ {
		for j := 0; j < len(Q); j++ {
			qi[j] = Q[j][i]
		}
		h[i] = matrixVecMult([][]float64{q}, qi)[0]
		q = vectorDif(q, vectNumMult(qi, h[i]))
	}
	h[k+1] = norm2(q)
	q = vectNumMult(q, 1.0/h[k+1])

	return h, q
}

func givensRotation(v1, v2 float64) (float64, float64) {
	var cs, sn float64
	if v1 == 0 {
		cs = 0
		sn = 1
	} else {
		t := math.Sqrt(math.Pow(v1, 2) + math.Pow(v2, 2))
		cs = math.Abs(v1) / t
		sn = cs * v2 / v1
	}

	return cs, sn
}

func applyGivensRotation(H [][]float64, cs, sn []float64, k int) {
	for i := 0; i < k; i++ {
		tmp := cs[i]*H[i][k] + sn[i]*H[i+1][k]
		H[i+1][k] = -sn[i]*H[i][k] + cs[i]*H[i+1][k]
		H[i][k] = tmp
	}

	cs[k], sn[k] = givensRotation(H[k][k], H[k+1][k])
	H[k][k] = cs[k]*H[k][k] + sn[k]*H[k+1][k]
	H[k+1][k] = 0
}

func gmres(A [][]float64, b []float64, eps float64, niters int) []float64 {
	x := make([]float64, len(b))
	r := vectorDif(b, matrixVecMult(A, x))

	bNorm := norm2(b)
	err := norm2(r) / bNorm

	sn := make([]float64, niters)
	cs := make([]float64, niters)

	rNorm := norm2(r)
	Q := make([][]float64, len(b))
	for i := 0; i < len(b); i++ {
		Q[i] = make([]float64, niters+1)
	}

	tmpVec := vectNumMult(r, 1.0/rNorm)

	H := make([][]float64, niters)
	for i := 0; i < niters; i++ {
		H[i] = make([]float64, niters)
	}
	for j := 0; j < len(tmpVec); j++ {
		Q[j][0] = tmpVec[j]
	}

	beta := make([]float64, niters+1)
	beta[0] = rNorm
	k := 0
	for ; (k < niters && err >= eps) || k < len(b); k++ {
		arnoldiH, arnoldiQ := arnoldi(A, Q, k)

		for i := 0; i < len(arnoldiH); i++ {
			H[i][k] = arnoldiH[i]
		}
		for i := 0; i < len(arnoldiQ); i++ {
			Q[i][k+1] = arnoldiQ[i]
		}

		applyGivensRotation(H, cs, sn, k)
		beta[k+1] = -sn[k] * beta[k]
		beta[k] = cs[k] * beta[k]
		err = math.Abs(beta[k+1]) / bNorm
	}

	//y = H(1:k,1:k) \ beta(1:k);
	//x = x + Q(:,1:k)*y;
	//bet

	beta = beta[:k]
	for i := 0; i < k; i++ {
		H[i] = H[i][:k]
	}
	H = H[:k]

	y := matrixVecMult(invertibleMatrix(H), beta)

	for i := 0; i < len(Q); i++ {
		Q[i] = Q[i][:k]
	}
	//x = x + Q(:,1:k)*y;
	x = vectorSum(x, matrixVecMult(Q, y))

	return x
}
