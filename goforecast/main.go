package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

type correctionFunction func([]float64, []float64)

/* func doTestForecast(coin, param string) {
	path := fmt.Sprintf("data/%s-%s.csv", coin, param)
	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("doTestForecast: err reading %s: %v\n", path, err)
	}

	lines := strings.Split(string(fileBytes), "\n")
	data := make([]float64, len(lines))
	for i, line := range lines {
		val, err := strconv.ParseFloat(line, 64)
		if err != nil {
			log.Printf("doTestForecast: err parsing value"+
				" from file %s on line %d: %v\n",
				path, i, err)
		}

		data[i] = val
	}

	trainIdx := int(0.7 * float64(len(data)))
	trainSet := data[:trainIdx]
	checkSet := data[trainIdx:]

	forecast := arModelForecast(trainSet, 10, len(checkSet))
	mape := 0.0
		mape += math.Abs((checkSet[i] - forecast[i]) / checkSet[i])
	}
	fmt.Printf("%s: AR mape: %f%%\n", path, mape)

} */

func doForecast(coin, param string, corrFunc correctionFunction) {
	path := fmt.Sprintf("data/%s-%s.csv", coin, param)
	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("doForecast: err reading %s: %v\n", path, err)
	}

	lines := strings.Split(string(fileBytes), "\n")
	data := make([]float64, len(lines))
	for i, line := range lines {
		val, err := strconv.ParseFloat(line, 64)
		if err != nil {
			log.Printf("doForecast: err parsing value"+
				" from file %s on line %d: %v\n",
				path, i, err)
		}

		data[i] = val
	}

	p := 30
	if len(data) < 30 {
		p = len(data) - 3
	}
	forecast := arModelForecast(data, p, 365)
	pathPredict := fmt.Sprintf("data/predict-%s-%s.csv", coin, param)
	file, err := os.Create(pathPredict)
	if err != nil {
		log.Printf("doRequest: err creating file %s: %v\n", pathPredict, err)
		return
	}

	lastVal := data[len(data)-1]
	fmt.Fprintf(file, "%.20f\n", lastVal)
	corrFunc(data, forecast)
	for i := 0; i < len(forecast); i++ {
		if !math.IsNaN(forecast[i]) {
			fmt.Fprintf(file, "%.20f\n", forecast[i])
		} else {
			fmt.Fprintf(file, "%.20f\n", lastVal)
		}
	}
}

func correctionMinMax(data []float64, mult float64) (float64, float64) {
	max := 0.0
	min := math.Inf(1)
	for i := 0; i < len(data); i++ {
		max = math.Max(data[i], max)
		min = math.Min(data[i], min)
	}
	return min / mult, max * mult
}

func correctionMaxInterval(data []float64, mult float64) (float64, float64) {
	max := 0.0
	for i := 0; i < len(data); i++ {
		max = math.Max(data[i], max)
	}
	return max / mult, max * mult
}

func main() {
	correction := func(data, forecast []float64) {
		min, max := correctionMaxInterval(data, 2.0)
		fmt.Printf("max=%f min=%f\n", max, min)

		minForecast, maxForecast := correctionMinMax(forecast, 1.0)
		fmt.Printf("maxForecast=%f minForecast=%f\n", maxForecast, minForecast)

		maxForecast -= minForecast
		mult := (max - min) / maxForecast
		fmt.Printf("mult=%f\n", mult)
		for i := 0; i < len(forecast); i++ {
			forecast[i] = (forecast[i]-minForecast)*mult + min
		}
	}

	priceCorrection := func(data, forecast []float64) {
		min, max := correctionMinMax(data, 1.1)

		minForecast, maxForecast := correctionMinMax(forecast, 1.0)

		maxForecast -= minForecast
		mult := (max - min) / maxForecast
		for i := 0; i < len(forecast); i++ {
			forecast[i] = (forecast[i]-minForecast)*mult + min
		}
	}

	coinsBytes, err := ioutil.ReadFile("public/resources/mining/coins.json")
	if err != nil {
		log.Fatalf("Err reading coins json: %v\n", err)
	}

	var coins []string
	err = json.Unmarshal(coinsBytes, &coins)
	if err != nil {
		log.Fatalf("Err parsing coins json: %v\n", err)
	}

	for _, coin := range coins {
		log.Println(coin)
		doForecast(coin, "blocktime", correction)
		doForecast(coin, "hashrate", correction)
		doForecast(coin, "price", priceCorrection)
	}
}
