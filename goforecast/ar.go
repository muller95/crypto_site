package main

import "math"

func autocorrelation(series []float64, p int) []float64 {
	r := make([]float64, p)
	for tau := 1; tau <= p; tau++ {
		sum1 := 0.0
		sum2 := 0.0
		sum3 := 0.0
		moment1 := 0.0
		moment2 := 0.0
		for i := tau; i < len(series); i++ {
			moment1 += series[i]
			moment2 += series[i-tau]
		}

		moment1 /= float64(len(series) - tau)
		moment2 /= float64(len(series) - tau)

		for i := tau; i < len(series); i++ {
			sum1 += (series[i] - moment1) * (series[i-tau] - moment2)
			sum2 += math.Pow(series[i]-moment1, 2)
			sum3 += math.Pow(series[i-tau]-moment2, 2)
		}

		denumerator := sum2 * sum3
		r[tau-1] = sum1 / math.Sqrt(denumerator)
	}

	return r
}

func calculateMoment(vals []float64, deg float64) float64 {
	sum := 0.0
	for i := 0; i < len(vals); i++ {
		sum += math.Pow(vals[i], deg)
	}

	return sum / float64(len(vals))
}

func calculateDispersion(data []float64) float64 {
	moment := calculateMoment(data, 1)
	sum := 0.0
	for i := 0; i < len(data); i++ {
		sum += math.Pow(data[i]-moment, 2)
	}
	return sum / float64(len(data)-1)
}

func arModelForecast(data []float64, p, n int) []float64 {
	series := make([]float64, len(data))
	for i := 0; i < len(series); i++ {
		series[i] = data[i]
	}
	moment := calculateMoment(series, 1)
	for i := 0; i < len(series); i++ {
		series[i] -= moment
	}
	r := autocorrelation(series, p)
	yuleWalker := make([][]float64, p)
	for i := 0; i < p; i++ {
		yuleWalker[i] = make([]float64, p)
		//for k, j, step := 0, i-1, -1; j < p; j, k = j+1, k+step {
		// for (int j = 0, k = i - 1, step = -1; j < p; j++, k += step) {
		for j, k, step := 0, i-1, -1; j < p; j, k = j+1, k+step {
			if i == j {
				k = -1
				step = 1
				yuleWalker[i][j] = 1
				continue
			}

			yuleWalker[i][j] = r[k]
		}
	}

	b := gmres(yuleWalker, r, 0.0000001, 1000)
	forecast := make([]float64, n)
	for t := 0; t < n; t++ {
		for j := 1; j <= p; j++ {
			if t-j >= 0 {
				forecast[t] += b[j-1] * forecast[t-j]
			} else {
				forecast[t] += b[j-1] * series[len(series)-(j-t)]
			}
		}
	}

	for i := 0; i < n; i++ {
		forecast[i] += moment
	}

	return forecast
}
